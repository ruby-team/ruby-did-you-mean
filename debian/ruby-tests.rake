require 'gem2deb/rake/testtask'

exclude = [
  'test/tree_spell/test_explore.rb', # takes too long
]

Gem2Deb::Rake::TestTask.new do |t|
  t.libs = ['test/lib', 'test']
  t.test_files = FileList['test/**/*_test.rb'] + FileList['test/**/test_*.rb'] - exclude
  t.ruby_opts = %w[ --disable-did_you_mean -rhelper ]
end

if v = ENV['DEB_VERSION_UPSTREAM']
  task :sanity_check do
    expected_version = Gem::Version.new(v)
    puts "Expected gem version: #{expected_version}"
    version = Gem::Specification.find_by_name('did_you_mean').version
    puts "Found gem version: #{version}"
    fail if version != Gem::Version.new(expected_version)
  end
  task :default => :sanity_check
end
