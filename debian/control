Source: ruby-did-you-mean
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Antonio Terceiro <terceiro@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1),
               rake
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-did-you-mean.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-did-you-mean
Homepage: https://github.com/ruby/did_you_mean
Testsuite: autopkgtest-pkg-ruby
Rules-Requires-Root: no

Package: ruby-did-you-mean
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Description: smart error messages for Ruby > 2.3
 This package provides the infrastructure to produce smart error messages on
 Ruby > 2.3.  When an undefined method is called, it will suggest similar
 method names. It will also catch mispellings on known method names, such as
 "initialize", and warn developers about them.
 .
 Starting with version 2.3, the Ruby interpreter will automatically load this
 library on startup.
